import VueRouter from 'vue-router'

import ArtistTable from '../components/artist/ArtistTable.vue'
import ArtistEditor from '../components/artist/ArtistEditor.vue'
import EventTable from '../components/event/EventTable.vue'
import EventEditor from '../components/event/EventEditor.vue'
import ReleaseTable from '../components/release/ReleaseTable.vue'
import ReleaseEditor from '../components/release/ReleaseEditor.vue'
import UserTable from '../components/user/UserTable.vue'
import UserEditor from '../components/user/UserEditor.vue'

const routes = [
   { path: '/', redirect: '/artist' },
   { path: '/artist', component: ArtistTable },
   { path: '/artist/:id', component: ArtistEditor, props: true },
   { path: '/event', component: EventTable },
   { path: '/event/:id', component: EventEditor, props: true },
   { path: '/release', component: ReleaseTable },
   { path: '/release/:id', component: ReleaseEditor, props: true },
   { path: '/user', component: UserTable },
   { path: '/user/:id', component: UserEditor, props: true },
]

export default new VueRouter({
   mode: 'history',
   routes
})
