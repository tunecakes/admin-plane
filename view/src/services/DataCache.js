/**
 * The structure of an item in the cache is as follow:
 * {
 *    expires: Date
 *    data: Cached Info
 * }
 *
 * Typically the cache will be keyed off of a mongo ID, but this doesn't alway have to be
 * the case. All cached info will expire after 10 minutes unless set otherwise. The data
 * itself should be stored as a Promise
 */

const cache = {}

function deleteCache(key) {
   delete(cache[key])
}

function lookupCache(key) {
   let hit = cache[key]
   let now = new Date()

   if (!hit) {
      return;
   }
   
   if (hit.expires < now) {
      return;
   }

   let returnedPromise, returnedResolve, returnedReject;
   let restoredPromise, restoredResolve, restoredReject;
   returnedPromise = new Promise(function(resolve, reject){
      returnedResolve = resolve;
      returnedReject = reject;
   });
   restoredPromise = new Promise(function(resolve, reject){
      restoredResolve = resolve;
      restoredReject = reject;
   });

   hit.data.then(d => {
      returnedResolve(d)
      restoredResolve(d)
   }).catch(e => {
      returnedReject(e)
      restoredReject(e)
   })

   cache[key].data = restoredPromise;
   return returnedPromise;
}

function storeCache(key, data, expires) {
   if (!expires) {
      expires = new Date()
      expires.setMinutes(expires.getMinutes() + 10)
   }

   cache[key] = {
      data,
      expires,
   }
}

export default {
   deleteCache,
   lookupCache,
   storeCache,
}
