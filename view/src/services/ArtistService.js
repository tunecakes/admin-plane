import Api from '../lib/Api.js'
import Cache from './DataCache.js'

export default {
   async getAll (page = 0, options) {
      let url = '/api/artists?' + 'p=' + page

      if (options) {
         url += '&q=' + JSON.stringify(options)
      }

      const res = await Api().get(url)
      return res.data;
   },
   async getById (id) {
      const hit = Cache.lookupCache(id)
      if (hit) {
         const cachedRes = await hit
         return cachedRes.data;
      }

      // Send request and store request promise in cache
      const resP = Api().get('/api/artists/' + id)
      Cache.storeCache(id, resP)
      
      // Use the cache to return the request
      const res = await Cache.lookupCache(id)
      return res.data;
   },
   async update (id, updatedArtist) {
      Cache.deleteCache(id);
      const res = await Api().put('/api/artists/' + id, updatedArtist)
      return res.data;
   },
}
