// Native libs
const path = require('path')

// NPM libs
const config = require('config')
const httpProxyMiddleware = require('http-proxy-middleware');
const mongoose = require('mongoose')
const restify = require('restify')
const restifyMongoose = require('restify-mongoose')

const envName = config.get("env")
const mongoURI = config.get('mongo_uri')
mongoose.connect(mongoURI)

let server = restify.createServer({
    name: 'restify.mongoose.tunecakes',
    version: '1.0.0'
})


// Configure the rest of the server
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())

// Prepare MongoDB schemas
const user = require('./models/user.js')
const artist = require('./models/artist.js')
const event = require('./models/event.js')
const release = require('./models/release.js')
const song = require('./models/song.js')
const post = require('./models/post.js')
const request = require('./models/request.js')

const User = mongoose.model(user.name, new mongoose.Schema(user.mongoose))
const Artist = mongoose.model(artist.name, new mongoose.Schema(artist.mongoose))
const Event = mongoose.model(event.name, new mongoose.Schema(event.mongoose))
const Release = mongoose.model(release.name, new mongoose.Schema(release.mongoose))
const Post = mongoose.model(post.name, new mongoose.Schema(post.mongoose))
const Song = mongoose.model(song.name, new mongoose.Schema(song.mongoose))
const Request = mongoose.model(request.name, new mongoose.Schema(request.mongoose))

// Now create restify-mongoose resources
const users = restifyMongoose(User)
const artists = restifyMongoose(Artist)
const events = restifyMongoose(Event)
const releases = restifyMongoose(Release)
const songs = restifyMongoose(Song)
const posts = restifyMongoose(Post)
const requests = restifyMongoose(Request)

// Serve resource notes with fine grained mapping control
server.post('/api/users', users.insert())
server.get('/api/users', users.query())
server.get('/api/users/:id', users.detail())
server.put('/api/users/:id', users.update())
server.del('/api/users/:id', users.remove())

server.post('/api/artists', artists.insert())
server.get('/api/artists', artists.query())
server.get('/api/artists/:id', artists.detail())
server.put('/api/artists/:id', artists.update())
server.del('/api/artists/:id', artists.remove())

server.post('/api/events', events.insert())
server.get('/api/events', events.query())
server.get('/api/events/:id', events.detail())
server.put('/api/events/:id', events.update())
server.del('/api/events/:id', events.remove())

server.post('/api/releases', releases.insert())
server.get('/api/releases', releases.query())
server.get('/api/releases/:id', releases.detail())
server.put('/api/releases/:id', releases.update())
server.del('/api/releases/:id', releases.remove())

server.post('/api/songs', songs.insert())
server.get('/api/songs', songs.query())
server.get('/api/songs/:id', songs.detail())
server.put('/api/songs/:id', songs.update())
server.del('/api/songs/:id', songs.remove())

server.post('/api/posts', posts.insert())
server.get('/api/posts', posts.query())
server.get('/api/posts/:id', posts.detail())
server.put('/api/posts/:id', posts.update())
server.del('/api/posts/:id', posts.remove())

server.post('/api/requests', requests.insert())
server.get('/api/requests', requests.query())
server.get('/api/requests/:id', requests.detail())
server.put('/api/requests/:id', requests.update())
server.del('/api/requests/:id', requests.remove())

if (envName === "development") {
   server.get('*', httpProxyMiddleware({target: 'http://localhost:8080'}));
} else {
   // Serve static files in production
   server.get('/css/*', restify.plugins.serveStatic({
      directory: path.join(__dirname, '../view/dist/'),
   }))
   server.get('/js/*', restify.plugins.serveStatic({
      directory: path.join(__dirname, '../view/dist/'),
   }))
   server.get('*', restify.plugins.serveStatic({
      directory: path.join(__dirname, '../view/dist/'),
      file: 'index.html',
   }))
}

server.listen(config.get('port'), function () {
    console.log('%s listening at %s', server.name, server.url)
})
