var Schema = require('mongoose').Schema;

var follow_map = {
   name: 'FollowMap',
   mongoose: {
      publisher_type: {type: String, trim: true},
      publisher: Schema.ObjectId,
      follower: { type: Schema.ObjectId, ref: 'user' },
      flags: [{type: String, trim: true}]
   }
};

module.exports = follow_map;
