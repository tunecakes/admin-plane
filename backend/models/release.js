var Schema = require('mongoose').Schema;

var release = {
   name: 'release',
   populate: [{
      path: 'artist',
      select: 'name alias id'
   }, {
      path: 'linked_artists',
      select: 'name'
   }, {
      path: 'songs',
      select: 'name length'
   }, ],
   mongoose: {
      // Standard stuff
      created: Date,
      updated: Date,
      admins: [{
         type: Schema.ObjectId,
         ref: 'user'
      }],
      owner: {
         type: Schema.ObjectId,
         ref: 'user'
      },

      name: String,
      type: {
         // eg Album, EP, LP
         type: String,
         default: 'album'
      },
      description: String,
      tags: [{
         type: String,
         trim: true
      }],
      image_link: String,
      audio_link: String,
      video_link: String,
      artist: {
         type: Schema.ObjectId,
         ref: 'artist'
      },
      linked_artists: [{
         type: Schema.ObjectId,
         ref: 'artist'
      }],
      unlinked_artists: [Schema.Types.Mixed],
      songs: [{
         type: Schema.ObjectId,
         ref: 'song'
      }],
      publish_date: {
         type: Date,
         default: ''
      },
      label: String,
      license: String,
      recording_studio: String,
      external_url: String,
      mbid: String,

      purchase_urls: [{
         media_type: String,
         url: String,
         vendor_name: String
      }],
   },
   permissions: {
      user: {
         admins: 'X',
         owner: 'X',
      },

      admin: {
         created: 'R',
         updated: 'R',
         admins: 'R',
         owner: 'R',
         artist: 'R',
         contributing_artists: 'R',
         songs: 'R',
      },

      owner: {
         created: 'R',
         updated: 'R',
         admins: 'R',
         owner: 'R',
         artist: 'R',
         contributing_artists: 'R',
         songs: 'R',
      }
   }
};

module.exports = release;
