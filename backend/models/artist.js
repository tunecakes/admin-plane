var Schema = require('mongoose').Schema;

var artist = {
   name: 'artist',
   populate: [{
      path: 'events',
      select: 'name start_date image_link type city state country zip'
   }, {
      path: 'owner',
      select: 'name email'
   }, {
      path: 'admins',
      select: 'name email'
   }],
   mongoose: {
      // Standard stuff
      created: Date,
      updated: Date,
      admins: [{
         type: Schema.ObjectId,
         ref: 'user'
      }],
      owner: {
         type: Schema.ObjectId,
         ref: 'user'
      },

      // System information
      contact_email: {
         type: String,
         trim: true
      },
      alias: {
         type: String,
         trim: true,
         unique: true,
         sparse: true
      },
      pending_admins: [Schema.Types.Mixed],

      // Artist Data
      name: {
         type: String,
         trim: true,
         required: true
      },
      start_date: Date,
      description: String,
      tags: {
         type: [String],
         trim: true
      },
      members: [Schema.Types.Mixed],
      events: [{
         type: Schema.Types.ObjectId,
         ref: 'event'
      }],
      releases: [{
         type: Schema.ObjectId,
         ref: 'release'
      }],
      release_contributions: [{
         type: Schema.ObjectId,
         ref: 'release'
      }],
      songs: {
         type: [Schema.ObjectId],
         ref: 'song'
      },
      posts: [{
         type: Schema.ObjectId,
         ref: 'post'
      }],

      // Media in profile
      image_link: String,
      audio_link: String,
      video_link: String,

      /*
       * External Accounts
       */
      official_site_url: String,
      spotify_id: String,
      spotify_url: String,
      facebook_id: String,
      facebook_url: String,
      twitter_id: String,
      twitter_url: String,
      mbid: String,
      bandcamp_url: String,

      // We want to keep this info hidden, especially from non-admin users
      // It's access tokens and login info for using their accounts
      // Should probably move this into its own model at some point
      social_accounts: {
         instagram: {},
         facebook: {},
         twitter: {}
      }
   },
   permissions: {
      user: {
         owner: 'X',
         admin: 'X',
         facebook: 'X',
         twitter: 'X',
      },

      admin: {
         owner: 'R',
         admin: 'R',
         created: 'R',
         updated: 'R',
         pending_admins: 'R',
         events: 'R',
         releases: 'R',
         songs: 'R',
      },

      owner: {
         owner: 'R',
         admin: 'R',
         created: 'R',
         updated: 'R',
         pending_admins: 'R',
         events: 'R',
         releases: 'R',
         songs: 'R',
      }
   }
};

module.exports = artist;
