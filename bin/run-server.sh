#! /bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
NODE_ENV=production

cd $DIR/../view/ && npm install && npm run build

cd $DIR/../backend/ && npm install
forever start app.js
